# Do the required imports for stopwords and tokenization
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

# a review...
review_ex = """This place is great! Atmosphere is chill and cool but the staff is also really friendly. 
They know what they’re doing and what they’re talking about, and you can tell making the customers happy is their main priority. 
Food is pretty good, some italian classics and some twists, and for their prices it’s 100% worth it."""

# create a set from the english stopword list.
stop_words_set = set(stopwords.words('english'))

# function
def filter_stopwords(some_text, stop_words_set):
    # tokenize the review
    word_tokens = word_tokenize(some_text)
    # filter stopwords
    filtered_text = [w for w in word_tokens if not w in stop_words_set]

    return filtered_text

print(review_ex)
print(filter_stopwords(review_ex, stop_words_set))
