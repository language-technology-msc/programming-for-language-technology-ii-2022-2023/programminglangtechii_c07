# we have to import stopwords from nltk.corpus
from nltk.corpus import stopwords

# for each language there is different list of stopwords
# let's print the english list
stopwords_list = stopwords.words('english')
print(stopwords_list)