"""
- Task: Create a simple tokenizer in Python.

- Idea: Use regular expressions (taught in Lang. Tech. I) to identify the tokens.

- web search: 'NLTK tokenize texts with regular expressions' ...
  RegexpTokenizer, https://www.nltk.org/_modules/nltk/tokenize/regexp.html

- Let's start the implementation
"""

# First import from the tokenizer package ('tokenize') of ntlk library the RegexpTokenizer class.
from nltk.tokenize import RegexpTokenizer

# Example text for testing the tokenizer that we will create.
string_to_tokenize = 'This is the first lesson of Language Technology II! It is Wednesday and the date is 24/2/2021. '


def print_tokens(reg_ex, str_to_tokenize):
    """
    A function that prints tokens.
    """

    # initialize RegexpTokenizer
    tokenizer = RegexpTokenizer(reg_ex)
    # tokenize
    tokenized_text = tokenizer.tokenize(string_to_tokenize)
    # print
    print(tokenized_text)

print(string_to_tokenize)

# RegexpTokenizer requires a regex pattern (a string) which will be used to identify the tokens for a piece of text.
# A token is sequence of characters and digits
# web search for 'python regular expressions' -> https://www.w3schools.com/python/python_regex.asp
# \w -> Returns a match where the string contains any word characters (characters from a to Z, digits from 0-9, and the underscore _ character)

# regular expression 1
reg_ex1 = r'\w'
print_tokens(reg_ex1, string_to_tokenize)
# Ouput: ['T', 'h', 'i', 's', 'i', 's', 't', 'h', 'e', 'f', ...

# regular expression 2
# add + which matches "One or more occurrences"
# https://www.w3schools.com/python/python_regex.asp
reg_ex2 = r'\w+'
print_tokens(reg_ex2, string_to_tokenize)
# ['This', 'is', 'the', 'first', 'lesson', 'of', 'Language', 'Technology', 'II', 'It', 'is', 'Wednesday', 'and', 'the', 'date', 'is', '24', '2', '2021']

# regular expression 3
reg_ex3 = '\w+'
print_tokens(reg_ex3, string_to_tokenize)
# ['This', 'is', 'the', 'first', 'lesson', 'of', 'Language', 'Technology', 'II', 'It', 'is', 'Wednesday', 'and', 'the', 'date', 'is', '24', '2', '2021']
# Same as before
# Why r  is required?
# https://www.flake8rules.com/rules/W605.html
# As of Python 3.6, a backslash-character pair that is not a valid escape sequence now generates a DeprecationWarning. This will eventually become a SyntaxError.

# One problem is that reg_ex2 does not return punctuation
# which are very useful. E.g
# "." for detecting sentences
# "," for parsing
# "!" for sentiment analysis
# So we need a more clever expression...
# Match any word/digit sequence OR punctuation
# word/digit sequence -> \w+
# OR -> |
# punctuation+special characters - >  [!.,#$&]
# More info on https://www.w3schools.com/python/python_regex.asp -> [arn]	Returns a match where one of the specified characters (a, r, or n) are present.
# regular expression 3
reg_ex4 = r'\w+|[!.,#$&-]'
string_to_tokenize = 'I am Dimitris. Today is 19-5-2021'
print_tokens(reg_ex4, string_to_tokenize)
# Our tokenizer is not perfect. It should return 19-5-2021

# More issues ...
string_to_tokenize = 'I have a Ph.D. I want to work for AT&T. A ticket train costs $30. '
print_tokens(reg_ex4, string_to_tokenize)
# ['I', 'have', 'a', 'Ph', '.', 'D', '.', 'I', 'want', 'ot', 'work', 'for', 'AT', '&', 'T', '.', 'A', 'ticket', 'train', 'costs', '$', '30', '.']
# Issues
# Ph.D
# AT&T
# $30 ===> ???